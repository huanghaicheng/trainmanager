#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qapplication.h"
#include "datasaving.h"
#include "data.h"

#include <QPainter>
#include <QProxyStyle>
#include <QMessageBox>
#include <QCloseEvent>
#include <QApplication>
#include <string>
#include <fstream>
#include <iomanip>
#include <QSettings>
#include <QTextCodec>
#include <iostream>
#include <QStandardItem>
#include <QBrush>

extern train *t_data;  //车次链表头结点
station *s_data;
station *s_r;  //尾指针

//以下代码为修改tab标签中的文字方向，来源自
//https://blog.csdn.net/skyztttt/article/details/52448992

class CustomTabStyle : public QProxyStyle
{
public:
    QSize sizeFromContents(ContentsType type, const QStyleOption *option,
        const QSize &size, const QWidget *widget) const
    {
        QSize s = QProxyStyle::sizeFromContents(type, option, size, widget);
        if (type == QStyle::CT_TabBarTab) {
            s.transpose();
            s.rwidth() = 350; // 设置每个tabBar中item的大小
            s.rheight() = 160;
        }
        return s;
    }

    void drawControl(ControlElement element, const QStyleOption *option, QPainter *painter, const QWidget *widget) const
    {
        if (element == CE_TabBarTabLabel) {
            if (const QStyleOptionTab *tab = qstyleoption_cast<const QStyleOptionTab *>(option)) {
                QRect allRect = tab->rect;

                if (tab->state & QStyle::State_Selected) {
                    painter->save();
                    painter->setPen(0x89cfff);
                    painter->setBrush(QBrush(0x89cfff));
                    painter->drawRect(allRect.adjusted(6, 6, -6, -6));
                    painter->restore();
                }
                QTextOption option;
                option.setAlignment(Qt::AlignCenter);
                if (tab->state & QStyle::State_Selected) {
                    painter->setPen(0xf8fcff);
                }
                else {
                    painter->setPen(0x5d5d5d);
                }
                painter->drawText(allRect, tab->text, option);
                return;
            }
        }

        if (element == CE_TabBarTab) {
            QProxyStyle::drawControl(element, option, painter, widget);
        }
    }
};


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->setTabPosition(QTabWidget::West);
        ui->tabWidget->tabBar()->setStyle(new CustomTabStyle);

    #if 0
        ui->tabWidget->setStyleSheet("QTabWidget::pane{ \
                border-left: 1px solid #eeeeee;\
            }");
    #endif
    //连接各种菜单点击事件
    connect(ui->About, SIGNAL(triggered()), this, SLOT(m_about()));
    connect(ui->Exit, SIGNAL(triggered()), this, SLOT(m_exit()));
    connect(ui->actionTrain, SIGNAL(triggered()), this, SLOT(m_train()));

    InitList(t_data);
    indata();
    ui->tabWidget->setCurrentIndex(0);

    //设置tableview
    model = new QStandardItemModel();
    model_s = new QStandardItemModel();
    model->setColumnCount(5);
    model->setHeaderData(0,Qt::Horizontal,QString::fromLocal8Bit("车次号"));
    model->setHeaderData(1,Qt::Horizontal,QString::fromLocal8Bit("始发站"));
    model->setHeaderData(2,Qt::Horizontal,QString::fromLocal8Bit("终点站"));
    model->setHeaderData(3,Qt::Horizontal,QString::fromLocal8Bit("硬座票价"));
    model->setHeaderData(4,Qt::Horizontal,QString::fromLocal8Bit("卧铺票价"));
    model_s->setColumnCount(5);
    model_s->setHeaderData(0,Qt::Horizontal,QString::fromLocal8Bit("站名"));
    model_s->setHeaderData(1,Qt::Horizontal,QString::fromLocal8Bit("到达时间"));
    model_s->setHeaderData(2,Qt::Horizontal,QString::fromLocal8Bit("开车时间"));
    model_s->setHeaderData(3,Qt::Horizontal,QString::fromLocal8Bit("天数"));
    model_s->setHeaderData(4,Qt::Horizontal,QString::fromLocal8Bit("里程"));
}

MainWindow::~MainWindow()
{
    delete ui;
    DestroyList(t_data);
}



////////////////////////////////////////功能函数///////////////////////////////////////////////


//读取数据
void MainWindow::indata(){
    QTextCodec *code = QTextCodec::codecForName("GB2312");//解决中文路径问题
    QSettings *pIni = new QSettings(QCoreApplication::applicationDirPath()+"/fileName.ini", QSettings::IniFormat);
    QString fileName;
    //读入ini文件
    fileName = pIni->value("/setting/arg1").toString();
    std::string name = code->fromUnicode(fileName).data();
    //读入数据文件
    ifstream infile;
    infile.open(name.c_str(),ios::in);
    if(!infile){
        QMessageBox::warning(this,tr("警告！"),tr("请正确选择数据文件位置！"),QMessageBox::Yes);
        return;
    }
    infile>>name;  //txt头内容,消去utf-8 bom乱码问题
    if(name.size()!=3){  //如果文件非空,bom字节数为3
        infile>>name;
        train *t;
        while(!infile.eof()){
            t = new train;
            station *s = new station,*r_s,*r=s;
            infile>>t->id>>t->start>>t->end>>t->h_price>>t->e_price;
                infile>>name;
                if(name!="end")
                {
                    while(name!="end")
                    {
                        r_s = new station;
                        r_s->s_id = name;
                        infile>>r_s->s_name>>r_s->arrival>>r_s->driving>>r_s->days>>r_s->mileage;
                        r->next_s = r_s;
                        r = r_s;
                        infile>>name;
                    }
                    r->next_s = NULL;
                    t->passing = s;
                }
            if(r->s_id.empty()) break;
            t->next_t = t_data->next_t;
            t_data->next_t = t;
        }
    }
    infile.close();
    delete pIni;
}

//写出数据
void MainWindow::outdata(){
    QTextCodec *code = QTextCodec::codecForName("GB2312");//解决中文路径问题
    QSettings *pIni = new QSettings(QCoreApplication::applicationDirPath()+"/fileName.ini", QSettings::IniFormat);
    QString fileName;
    //读入ini文件
    fileName = pIni->value("/setting/arg1").toString();
    std::string name = code->fromUnicode(fileName).data();
    ofstream outfile;
    outfile.open(name.c_str(),ios::out);
    if(!outfile){
        QMessageBox::warning(this,tr("警告！"),tr("打开文件失败!"),QMessageBox::Yes);
        return;
    }
    outfile<<"TrainManager"<<endl;
    outfile<<"客运列车信息记录"<<endl;
    outfile<<endl;
    train *t = t_data->next_t;
    station *s;
    while(t!=NULL){
        outfile<<t->id<<std::setw(16)<<t->start;
        outfile<<std::setw(16)<<t->end<<std::setw(8)<<t->h_price;
        outfile<<std::setw(8)<<t->e_price<<endl;
        s = t->passing;
        s = s->next_s;
        while (s!=NULL) {
            outfile<<s->s_id<<std::setw(20)<<s->s_name;
            outfile<<std::setw(20)<<s->arrival;
            outfile<<std::setw(20)<<s->driving<<std::setw(10)<<s->days;
            outfile<<std::setw(10)<<s->mileage<<endl;
            s = s->next_s;
        }
        outfile<<"end"<<endl;
        t = t->next_t;
    }
    outfile.close();
    delete pIni;
}



////////////////////////////////////////菜单事件///////////////////////////////////////////////



//菜单点击帮助->关于事件
void MainWindow::m_about()
{
    QMessageBox m(this);
    m.setWindowTitle(tr("About"));
    m.setText(tr("\n本软件为黄海乘版权所有，仅供学习交流，侵权不究。\n\n\n GitLab地址：https://gitlab.com/huanghaicheng/trainmanager"));
    m.exec();

}

//菜单点击退出事件
void MainWindow::m_exit()
{
    this->close();
}
//退出前的一波操作
void MainWindow::closeEvent(QCloseEvent *Event)
{
    int r = QMessageBox::information(this,tr("请选择..."),tr("确定退出？"),QObject::tr("确定"),QObject::tr("取消"));
    if(r == QObject::tr(("确定")).toInt()){
        outdata();
        Event->accept();
    }else{
        Event->ignore();
    }

}

//菜单点击车次数据事件
void MainWindow::m_train()
{
    Datasaving d;
    d.exec();
}



//页面切换事件
void MainWindow::on_tabWidget_currentChanged(int index)
{
    InitList(s_data);
    if(index==1)
    {
        s_r = s_data;
        on_Btn_reset_clicked();
    }else if(index==3){
        ui->lineEdit->clear();
        ui->lineEdit_2->clear();
        FindReset();
    }else if(index==4){
        ui->lineEdit_id->clear();
        on_Btn_reset_p_clicked();
        t_mod = NULL;
    }else if(index==2){
        ui->btn_SeeStation->setEnabled(true);
        setmodel();
        setmodeldata();
    }
}


////////////////////////////////////////添加页面///////////////////////////////////////////////

//添加页面重置按钮
void MainWindow::on_Btn_reset_clicked()
{
    //清空输入框
    ui->LE_arrivals->clear();
    ui->LE_days->clear();
    ui->LE_driving->clear();
    ui->LE_end->clear();
    ui->LE_hardseat->clear();
    ui->LE_id->clear();
    ui->LE_mileage->clear();
    ui->LE_sleeper->clear();
    ui->LE_start->clear();
    ui->LE_station->clear();
    ui->LE_stationName->clear();
    //允许车次信息写入
    ui->LE_id->setEnabled(true);
    ui->LE_start->setEnabled(true);
    ui->LE_end->setEnabled(true);
    ui->LE_hardseat->setEnabled(true);
    ui->LE_sleeper->setEnabled(true);
    //删除站点数据
    DestroyList(s_data);
    InitList(s_data);
}

//添加页面继续添加按钮·
void MainWindow::on_Btn_continue_clicked()
{
    if(s_data->next_s==NULL) s_r = s_data;
    string str1,str2,str3,str4,str5,str6,str7,str8,str9,str10,str11;
    str1 = std::string((const char *)(ui->LE_id->text()).toLocal8Bit());  //车次号
    str2 = std::string((const char *)(ui->LE_start->text()).toLocal8Bit());  //始发站
    str3 = std::string((const char *)(ui->LE_end->text()).toLocal8Bit());  //终点站
    str4 = std::string((const char *)(ui->LE_hardseat->text()).toLocal8Bit());  //硬座票价
    str5 = std::string((const char *)(ui->LE_sleeper->text()).toLocal8Bit());  //卧铺票价
    str6 = std::string((const char *)(ui->LE_station->text()).toLocal8Bit());  //站次
    str7 = std::string((const char *)(ui->LE_stationName->text()).toLocal8Bit());  //站名
    str8 = std::string((const char *)(ui->LE_arrivals->text()).toLocal8Bit());  //到达时间
    str9 = std::string((const char *)(ui->LE_driving->text()).toLocal8Bit());  //开车时间
    str10 = std::string((const char *)(ui->LE_days->text()).toLocal8Bit());  //天数
    str11 = std::string((const char *)(ui->LE_mileage->text()).toLocal8Bit());  //里程
    if(!str1.empty()&&!str2.empty()&&!str3.empty()&&!str4.empty()&&!str5.empty()&&
            !str6.empty()&&!str7.empty()&&!str8.empty()&&!str9.empty()&&!str10.empty()&&!str11.empty())
    {
        //尾插法插入到站点链表
        station *s = new station;
        s->s_id = str6;
        s->s_name = str7;
        s->arrival = str8;
        s->driving = str9;
        s->days = str10;
        s->mileage = str11;
        s_r->next_s = s;
        s_r = s;
        ui->LE_arrivals->clear();
        ui->LE_days->clear();
        ui->LE_driving->clear();
        ui->LE_mileage->clear();
        ui->LE_station->clear();
        ui->LE_stationName->clear();
    }else{
        QMessageBox::warning(this,tr("警告！"),tr("请完整输入！"),QMessageBox::Yes);
        return;
    }
    if(!str1.empty()&&!str2.empty()&&!str3.empty()&&!str4.empty()&&!str5.empty())
    {
        ui->LE_id->setEnabled(false);
        ui->LE_start->setEnabled(false);
        ui->LE_end->setEnabled(false);
        ui->LE_hardseat->setEnabled(false);
        ui->LE_sleeper->setEnabled(false);
    }
}


//添加页面确定按钮
void MainWindow::on_Btn_OK_clicked()
{
    if(ui->LE_id->isEnabled()==true){
        QMessageBox::warning(this,tr("警告！"),tr("添加失败！"),QMessageBox::Yes);
        return;
    }
    string str1,str2,str3,str4,str5;
    str1 = std::string((const char *)(ui->LE_id->text()).toLocal8Bit());  //车次号
    str2 = std::string((const char *)(ui->LE_start->text()).toLocal8Bit());  //始发站
    str3 = std::string((const char *)(ui->LE_end->text()).toLocal8Bit());  //终点站
    str4 = std::string((const char *)(ui->LE_hardseat->text()).toLocal8Bit());  //硬座票价
    str5 = std::string((const char *)(ui->LE_sleeper->text()).toLocal8Bit());  //卧铺票价
    //头插法插入到车次链表
    train *t = new train;
    t->id = str1;
    t->start = str2;
    t->end = str3;
    t->h_price = str4;
    t->e_price = str5;
    s_r->next_s = NULL;
    t->passing = s_data;
    t->next_t = t_data->next_t;
    t_data->next_t = t;
    //InitList(s_data);
    int r = QMessageBox::information(this,tr("提醒！"),tr("添加成功！"),QObject::tr("确定"));
    if (r == QObject::tr(("确定")).toInt())
    {
        //清空输入框
        ui->LE_arrivals->clear();
        ui->LE_days->clear();
        ui->LE_driving->clear();
        ui->LE_end->clear();
        ui->LE_hardseat->clear();
        ui->LE_id->clear();
        ui->LE_mileage->clear();
        ui->LE_sleeper->clear();
        ui->LE_start->clear();
        ui->LE_station->clear();
        ui->LE_stationName->clear();
        //允许车次信息写入
        ui->LE_id->setEnabled(true);
        ui->LE_start->setEnabled(true);
        ui->LE_end->setEnabled(true);
        ui->LE_hardseat->setEnabled(true);
        ui->LE_sleeper->setEnabled(true);
    }
}



////////////////////////////////////////查询页面///////////////////////////////////////////////


//车次查询按钮
void MainWindow::on_pushButton_2_clicked()
{
    string str;
    QString qstr;
    str = std::string((const char *)(ui->lineEdit->text()).toLocal8Bit());
    FindReset();
    ui->lineEdit_2->clear();
    if(!str.empty()){
        train *t = LocateElem(t_data,str);
        if(t==NULL){
            QMessageBox::warning(this,tr("警告！"),tr("没有此车次！"),QMessageBox::Yes);
            ui->lineEdit->clear();
        }else{
            s_find = t->passing;
            str = t->start;
            ui->label_id->setText(QString::fromStdString(t->id));
            ui->label_start->setText(QString(QString::fromLocal8Bit(t->start.c_str())));
            ui->label_end->setText(QString::fromStdString(t->end));
            ui->label_hardseat->setText(QString::fromStdString(t->h_price));
            ui->label_sleeper->setText(QString::fromStdString(t->e_price));
            qstr = ui->label_start->text();
        }
    }else{
        QMessageBox::warning(this,tr("警告！"),tr("请输入车次号！"),QMessageBox::Yes);
    }
}

//重置函数
void MainWindow::FindReset(){
    ui->label_id->clear();
    ui->label_start->clear();
    ui->label_end->clear();
    ui->label_hardseat->clear();
    ui->label_sleeper->clear();
    ui->label_station->clear();
    ui->label_stationName->clear();
    ui->label_arrivals->clear();
    ui->label_driving->clear();
    ui->label_days->clear();
    ui->label_mileage->clear();
}

//站点查询按钮
void MainWindow::on_pushButton_clicked()
{
    string str;
    str = std::string((const char *)(ui->lineEdit_2->text()).toLocal8Bit());
    ui->label_station->clear();
    ui->label_stationName->clear();
    ui->label_arrivals->clear();
    ui->label_driving->clear();
    ui->label_days->clear();
    ui->label_mileage->clear();
    if(!str.empty()){
        station *t = LocateElem(s_find,str);
        if(t==NULL){
            QMessageBox::warning(this,tr("警告！"),tr("没有此站点！"),QMessageBox::Yes);
            ui->lineEdit_2->clear();
        }else{
            ui->label_station->setText(QString(QString::fromLocal8Bit(t->s_id.c_str())));
            ui->label_stationName->setText(QString(QString::fromLocal8Bit(t->s_name.c_str())));
            ui->label_arrivals->setText(QString(QString::fromLocal8Bit(t->arrival.c_str())));
            ui->label_driving->setText(QString(QString::fromLocal8Bit(t->driving.c_str())));
            ui->label_days->setText(QString(QString::fromLocal8Bit(t->days.c_str())));
            ui->label_mileage->setText(QString::fromLocal8Bit(t->mileage.c_str()));
        }
    }else{
        QMessageBox::warning(this,tr("警告！"),tr("请输入站点名！"),QMessageBox::Yes);
    }
}



////////////////////////////////////////删改页面///////////////////////////////////////////////





//重置按钮
void MainWindow::on_Btn_reset_p_clicked()
{
    ui->LE_end_p->clear();
    ui->LE_hardseat_p->clear();
    ui->LE_sleeper_p->clear();
    ui->LE_start_p->clear();
    //clear无法清除placeholdertext
    ui->LE_end_p->setPlaceholderText("");
    ui->LE_hardseat_p->setPlaceholderText("");
    ui->LE_sleeper_p->setPlaceholderText("");
    ui->LE_start_p->setPlaceholderText("");
    t_mod = NULL;
}

//查询按钮
void MainWindow::on_pushButton_findId_clicked()
{
    string str = std::string((const char *)(ui->lineEdit_id->text()).toLocal8Bit());
    on_Btn_reset_p_clicked();
    if(!str.empty()){
        train *t = LocateElem(t_data,str);
        if(t==NULL){
            QMessageBox::warning(this,tr("警告！"),tr("没有此车次！"),QMessageBox::Yes);
        }else{
            t_mod = t;
            ui->LE_end_p->setPlaceholderText(QString::fromStdString(t->end));
            ui->LE_hardseat_p->setPlaceholderText(QString::fromStdString(t->h_price));
            ui->LE_sleeper_p->setPlaceholderText(QString::fromStdString(t->e_price));
            ui->LE_start_p->setPlaceholderText(QString::fromStdString(t->start));
        }
    }else{
        QMessageBox::warning(this,tr("警告！"),tr("请输入车次号！"),QMessageBox::Yes);
    }
}

//删除按钮
void MainWindow::on_Btn_delete_p_clicked()
{
    if(t_mod==NULL){
        QMessageBox::warning(this,tr("警告！"),tr("删除失败"),QMessageBox::Yes);
    }else{
        ListDelete(t_data,t_mod);
        int r = QMessageBox::information(this,tr("提醒！"),tr("删除成功！"),QObject::tr("确定"));
        if (r == QObject::tr(("确定")).toInt())
        {
            on_Btn_reset_p_clicked();
        }
    }
}

//修改按钮
void MainWindow::on_Btn_modify_p_clicked()
{
    if(t_mod==NULL){
        QMessageBox::warning(this,tr("警告！"),tr("修改失败"),QMessageBox::Yes);
    }else{
        string str2,str3,str4,str5;
        str2 = std::string((const char *)(ui->LE_start_p->text()).toLocal8Bit());  //始发站
        str3 = std::string((const char *)(ui->LE_end_p->text()).toLocal8Bit());  //终点站
        str4 = std::string((const char *)(ui->LE_hardseat_p->text()).toLocal8Bit());  //硬座票价
        str5 = std::string((const char *)(ui->LE_sleeper_p->text()).toLocal8Bit());  //卧铺票价
        if(!str2.empty()) t_mod->start = str2;
        if(!str3.empty()) t_mod->end = str3;
        if(!str4.empty()) t_mod->h_price = str4;
        if(!str5.empty()) t_mod->e_price = str5;
        int r = QMessageBox::information(this,tr("提醒！"),tr("删除成功！"),QObject::tr("确定"));
        if (r == QObject::tr(("确定")).toInt())
        {
            on_Btn_reset_p_clicked();
        }
    }
}



////////////////////////////////////////浏览页面///////////////////////////////////////////////



//设置tabelview数据源model样式
void MainWindow::setmodel(){
    ui->tableView->clearSpans();
    model->removeRows(0,model->rowCount());
    ui->tableView->setModel(model);
    //表头信息显示居左
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    //设置列宽不可变
    ui->tableView->setColumnWidth(0,160);
    ui->tableView->setColumnWidth(1,160);
    ui->tableView->setColumnWidth(2,160);
    ui->tableView->setColumnWidth(3,160);
    ui->tableView->setColumnWidth(4,160);
    //只读
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

//设置tabelview数据源model_s样式
void MainWindow::setmodel_s(){
    ui->tableView->clearSpans();
    model_s->removeRows(0,model_s->rowCount());
    ui->tableView->setModel(model_s);
    //表头信息显示居左
    ui->tableView->horizontalHeader()->setDefaultAlignment(Qt::AlignLeft);
    //设置列宽不可变
    ui->tableView->setColumnWidth(0,160);
    ui->tableView->setColumnWidth(1,160);
    ui->tableView->setColumnWidth(2,160);
    ui->tableView->setColumnWidth(3,160);
    ui->tableView->setColumnWidth(4,160);
    //只读
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //设置选中时为整行选中
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

//填充model数据
void MainWindow::setmodeldata(){
    ui->tableView->clearSpans();
    train *t = t_data->next_t;
    int j = 0;  //行数
    while(t!=NULL){
        //按列添加
        model->setItem(j,0,new QStandardItem(QString::fromStdString(t->id)));
        model->setItem(j,1,new QStandardItem(QString::fromStdString(t->start)));
        model->setItem(j,2,new QStandardItem(QString::fromStdString(t->end)));
        model->setItem(j,3,new QStandardItem(QString::fromStdString(t->h_price)));
        model->setItem(j,4,new QStandardItem(QString::fromStdString(t->e_price)));
        t = t->next_t;
        j++;
    }
}

//填充model_s数据
void MainWindow::setmodel_sdata(train *t){
    station *s = t->passing;
    s = s->next_s;
    int j = 0;  //行数
    while(s!=NULL){
        //按列添加
        model_s->setItem(j,0,new QStandardItem(QString::fromStdString(s->s_name)));
        model_s->setItem(j,1,new QStandardItem(QString::fromStdString(s->arrival)));
        model_s->setItem(j,2,new QStandardItem(QString::fromStdString(s->driving)));
        model_s->setItem(j,3,new QStandardItem(QString::fromStdString(s->days)));
        model_s->setItem(j,4,new QStandardItem(QString::fromStdString(s->mileage)));
        s = s->next_s;
        j++;
    }
    model_s->setItem(j,0,new QStandardItem(QString::fromStdString(t->id)));
    model_s->setItem(j,1,new QStandardItem(QString::fromStdString(t->start)));
    model_s->setItem(j,2,new QStandardItem(QString::fromStdString(t->end)));
    model_s->setItem(j,3,new QStandardItem(QString::fromStdString(t->h_price)));
    model_s->setItem(j,4,new QStandardItem(QString::fromStdString(t->e_price)));
    for(int i=0;i<5;i++){
        model_s->item(j,i)->setBackground(QBrush(QColor(135,206,235)));
    }
}

//取消按钮
void MainWindow::on_btn_cancel_clicked()
{
    ui->btn_SeeStation->setEnabled(true);
    setmodel();
    setmodeldata();
}

//查看站点按钮
void MainWindow::on_btn_SeeStation_clicked()
{
    int row = ui->tableView->currentIndex().row()+1;
    if(row==0){
        QMessageBox::warning(this,tr("警告！"),tr("请选择车次"),QMessageBox::Yes);
        return;
    }
    ui->btn_SeeStation->setEnabled(false);
    train *t = GetElem(t_data,row);
    setmodel_s();
    setmodel_sdata(t);
}
