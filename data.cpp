#include "data.h"
#include <string>
#include <QString>

void InitList(train *&L){
    L = new train;
    L->next_t = NULL;
}

void InitList(station *&L){
    L = new station;
    L->next_s = NULL;
}

void DestroyList(train *&L){
    train *pre = L,*p = L->next_t;
    while(p!=NULL){
        delete pre;
        pre = p;
        p = pre->next_t;
    }
    delete pre;
}

void DestroyList(station *&L){
    station *pre = L,*p = L->next_s;
    while(p!=NULL){
        delete pre;
        pre = p;
        p = pre->next_s;
    }
    delete pre;
}

bool ListEmpty(train *L){
    return(L->next_t==NULL);
}



train* GetElem(train *L, int i){
    int j=0;
    train *p = L;
    //if(i<=0) return false;
    while(j<i&&p!=NULL){
        j++;
        p = p->next_t;
    }
    if(p==NULL) return false;
    else{
        return p;
    }
}

train *LocateElem(train *L, string t_id){
    train *p = L->next_t;
    QString str1,str2;
    str1 = QString::fromStdString(t_id).toUpper();
    while(p!=NULL){
        str2 = QString::fromStdString(p->id).toUpper();
        if(str1==str2){
            return p;
        }
        p = p->next_t;
    }
    return NULL;
}

station *LocateElem(station *L, string s_id){
    station *p = L->next_s;
    while(p!=NULL&&p->s_name!=s_id){
        p = p->next_s;
    }
    if(p==NULL) return NULL;
    else return p;
}


//删除头结点为t的L结点
void ListDelete(train *&t, train *&L){
    train *p = t;
    station *s = L->passing;
    while(p->next_t!=L){
        p = p->next_t;
    }
    p->next_t = L->next_t;
    DestroyList(s);
    delete L;
}
