#ifndef DATA_H
#define DATA_H
#include <string>

//数据定义
using namespace std;

//站点单链表
struct station
{
    string s_id;  //站次
    string s_name;  //站名
    string arrival;  //到达时间
    string driving;  //出发时间
    string days;  //天数
    string mileage;  //里程
    station *next_s;  //下一个结点
};
//车次单链表
struct train
{
    string id;  //车次号
    string start;  //始发站
    string end;  //终点站
    string h_price;  //硬座票价
    string e_price;  //卧座票价
    train *next_t;  //下一个结点
    station *passing;  //途径站点
};

//运算声明
void InitList(train *&L);  //初始化
void InitList(station *&L);
void DestroyList(train *&L); // 销毁
void DestroyList(station *&L);
bool ListEmpty(train *L);
train *GetElem(train *L, int i);
train* LocateElem(train *L, string t_id);  //查找车次
station* LocateElem(station *L, string s_id);  //查找站点
void ListDelete(train *&t, train *&L);

#endif // DATA_H
