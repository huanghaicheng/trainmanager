#include "datasaving.h"
#include "ui_datasaving.h"
#include <fstream>
#include <string>
#include <QFileDialog>
#include <QSettings>
#include <QMessageBox>
#include <QFileInfo>
#include <QTextCodec>
#include <QCloseEvent>
#include "mainwindow.h"
#include "data.h"

train *t_data;

Datasaving::Datasaving(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Datasaving)
{
    QString fileName;
    QFileInfo fileInfo(QCoreApplication::applicationDirPath()+"/fileName.ini");
    ui->setupUi(this);
    ui->lineEdit->setFocusPolicy(Qt::NoFocus);   //文件路径显示LE不可编辑
    if(fileInfo.isFile()){
        QSettings *pIni = new QSettings(QCoreApplication::applicationDirPath()+"/fileName.ini", QSettings::IniFormat);
        fileName = pIni->value("/setting/arg1").toString();
        ui->lineEdit->setText(fileName);
        delete pIni;
    }
}

Datasaving::~Datasaving()
{
    delete ui;
}

void Datasaving::on_pushButton_clicked()
{
    QString fileName;
    QFileDialog q(this);
    QFileInfo fileInfo(QCoreApplication::applicationDirPath()+"/fileName.ini");
    if(!fileInfo.isFile()){
        QSettings *pIni = new QSettings(QCoreApplication::applicationDirPath()+"/fileName.ini", QSettings::IniFormat);
        fileName = q.getOpenFileName(this,tr("Open TXT File"),QCoreApplication::applicationDirPath(), tr("TXT Files (*.txt)"));
        ui->lineEdit->setText(fileName);
        if(!fileName.isEmpty())  pIni->setValue("/setting/arg1",fileName);
        else pIni->setValue("/setting/arg1","");
        delete pIni;
    }else{
        QSettings *pIni = new QSettings(QCoreApplication::applicationDirPath()+"/fileName.ini", QSettings::IniFormat);
        fileName = pIni->value("/setting/arg1").toString();
        if(!fileName.isEmpty()){
            fileName = q.getOpenFileName(this,tr("Open TXT File"),fileName,tr("TXT Files (*.txt)"));
            ui->lineEdit->setText(fileName);
            pIni->setValue("/setting/arg1",fileName);
        }else{
            fileName = q.getOpenFileName(this,tr("Open TXT File"),QCoreApplication::applicationDirPath(),tr("TXT Files (*.txt)"));
            ui->lineEdit->setText(fileName);
            pIni->setValue("/setting/arg1",fileName);
        }
        delete pIni;
    }
}

//退出前的一波操作
void Datasaving::closeEvent(QCloseEvent *Event)
{
    std::string str;
    str = std::string((const char *)(ui->lineEdit->text()).toLocal8Bit());
    if(!str.empty()){
        Event->accept();
    }else{
        QMessageBox::warning(this,tr("警告！"),tr("请输入存储地址！"),QMessageBox::Yes);
        Event->ignore();
    }

}

//读取数据按钮
void Datasaving::on_btn_read_clicked()
{
    MainWindow *m;
    DestroyList(t_data);
    InitList(t_data);
    m->indata();
}
