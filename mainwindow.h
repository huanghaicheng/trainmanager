#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "data.h"
#include <QStandardItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void indata();
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    station *s_find;
    train *t_mod;
    QStandardItemModel  *model;  //tableview数据源
    QStandardItemModel  *model_s;  //tableview数据源
private slots:
    void m_about();
    void m_exit();
    void m_train();

    void on_Btn_reset_clicked();

    void on_Btn_continue_clicked();

    void on_tabWidget_currentChanged(int index);

    void on_Btn_OK_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_Btn_reset_p_clicked();

    void on_pushButton_findId_clicked();

    void on_Btn_delete_p_clicked();

    void on_Btn_modify_p_clicked();

    void on_btn_cancel_clicked();

    void on_btn_SeeStation_clicked();

protected:
    void closeEvent(QCloseEvent *Event);
    void FindReset();
    void outdata();
    void setmodel();
    void setmodel_s();
    void setmodeldata();
    void setmodel_sdata(train *t);

};

#endif // MAINWINDOW_H
