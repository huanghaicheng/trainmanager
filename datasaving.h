#ifndef DATASAVING_H
#define DATASAVING_H

#include <QDialog>

namespace Ui {
class Datasaving;
}

class Datasaving : public QDialog
{
    Q_OBJECT

public:
    explicit Datasaving(QWidget *parent = 0);
    ~Datasaving();

private slots:
    void on_pushButton_clicked();

    void on_btn_read_clicked();

private:
    Ui::Datasaving *ui;

protected:
    void closeEvent(QCloseEvent *Event);
};

#endif // DATASAVING_H
