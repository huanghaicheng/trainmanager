#-------------------------------------------------
#
# Project created by QtCreator 2019-02-02T18:14:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TrainManager
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    datasaving.cpp \
    data.cpp

HEADERS  += mainwindow.h \
    datasaving.h \
    data.h

FORMS    += mainwindow.ui \
    datasaving.ui

RESOURCES += \
    trainpic.qrc
